class Doctor < ApplicationRecord
    validates :doctor_code, presence: true
    validates :doctor_name, presence: true
end
