class DoctorsController < ApplicationController
  def index
    doctors = Doctor.all
    count_doctor = doctors.count
    if count_doctor > 0
      msg = {:status => 200, :message => "Success", :data => doctors}
    else
      msg = {:status => 204, :message => "No data"}
    end
    render json: msg
  end

  def create
    #do something
    doctor = Doctor.new(doctor_params)

    if doctor.save
      msg = {:status => 201, :message => "Created"}
    else
      msg = {:status => :unprocessable_entity, :message => doctor.errors}
    end
    render json: msg
  end

  def destroy
    #do something
    doctor = Doctor.find(params[:id])
    doctor.destroy!
    msg = {:status => 200, :message => "Success"}
    render json: msg
  end
  private 
  def doctor_params
    params.require(:doctor).permit(:doctor_code, :doctor_name)
  end
end
