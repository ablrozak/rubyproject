class ApplicationController < ActionController::API
rescue_from ActiveRecord::RecordNotFound, with: :not_founded
rescue_from ActiveRecord::RecordNotDestroyed, with: :not_destroyed

private
def not_founded(e)
    msg = {:status => 204, :message => e}
    render json: msg
end

def not_destroyed(e)
    msg = {:status => :unprocessable_entity, :message => e.record.errors}
    render json: msg
end
end
